package com.api.parkingcontrol.controllers;

import com.api.parkingcontrol.dtos.VagaEstacionamentoDto;
import com.api.parkingcontrol.models.VagaEstacionamentoModel;
import com.api.parkingcontrol.services.VagaEstacionamentoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.hateoas.server.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@CrossOrigin(origins = "*", maxAge = 36000)
@RequestMapping("vagaEstacionamento")
public class VagaEstacionamentoController {

    @Autowired
    private VagaEstacionamentoService vagaEstacionamentoService;

    @PostMapping
    public ResponseEntity<Object> salvar(@RequestBody @Valid VagaEstacionamentoDto vagaEstacionamentoDto) {
        if (vagaEstacionamentoService.jaExisteComEssaPlacaDeVeiculo(vagaEstacionamentoDto.getPlacaVeiculo())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Já existe uma vaga de estacionamento registrada para essa Placa de Veiculo.");
        }

        if (vagaEstacionamentoService.jaExisteComEsseNumero(vagaEstacionamentoDto.getNumero())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Já existe uma vaga de estacionamento registrada com esse Numero.");
        }

        if (vagaEstacionamentoService.jaExisteParaEsseBlocoNoApartamento(vagaEstacionamentoDto.getApartamento(), vagaEstacionamentoDto.getBlocoApartamento())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Já existe uma vaga de estacionamento registrada para esse bloco no apartamento.");
        }

        var vagaEstacionamento = new VagaEstacionamentoModel();

        BeanUtils.copyProperties(vagaEstacionamentoDto, vagaEstacionamento);
        vagaEstacionamento.setDataRegistro(LocalDateTime.now(ZoneId.of("UTC")));

        return ResponseEntity.status(HttpStatus.CREATED).body(vagaEstacionamentoService.salvar(vagaEstacionamento));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> alterar(@PathVariable("id") UUID id,
                                          @RequestBody @Valid VagaEstacionamentoDto vagaEstacionamentoDto) {

        Optional<VagaEstacionamentoModel> vagaEstacionamentoOptional = vagaEstacionamentoService.buscarPorId(id);

        if (vagaEstacionamentoOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Vaga de estacionamento inexistente.");
        }

        var vagaEstacionamento = vagaEstacionamentoOptional.get();

        BeanUtils.copyProperties(vagaEstacionamentoDto, vagaEstacionamento);
        vagaEstacionamento.setId(vagaEstacionamento.getId());
        vagaEstacionamento.setDataRegistro(vagaEstacionamento.getDataRegistro());

        return ResponseEntity.status(HttpStatus.OK).body(vagaEstacionamentoService.salvar(vagaEstacionamento));
    }


    @GetMapping
    public ResponseEntity<Page<VagaEstacionamentoModel>> buscarTodas(@PageableDefault(page = 0, size = 10, sort = "id", direction = Sort.Direction.ASC) Pageable pageable) {
        Page<VagaEstacionamentoModel> pageVagasEstacionamentos = vagaEstacionamentoService.buscarTodas(pageable);

        pageVagasEstacionamentos.getContent().forEach(vagaEstacionamentoModel -> vagaEstacionamentoModel.add(linkTo(methodOn(VagaEstacionamentoController.class).buscarPorId(vagaEstacionamentoModel.getId())).withSelfRel()));

        return ResponseEntity.status(HttpStatus.OK).body(pageVagasEstacionamentos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> buscarPorId(@PathVariable("id") UUID id) {
        Optional<VagaEstacionamentoModel> vagaEstacionamentoOptional = vagaEstacionamentoService.buscarPorId(id);

        if (vagaEstacionamentoOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Vaga de estacionamento inexistente.");
        }

        vagaEstacionamentoOptional.get().add(linkTo(methodOn(VagaEstacionamentoController.class).buscarTodas(PageRequest.of(0, 1))).withRel("Lista de Vagas de Estacionamento"));

        return ResponseEntity.status(HttpStatus.OK).body(vagaEstacionamentoOptional.get());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> excluirPorId(@PathVariable("id") UUID id) {
        Optional<VagaEstacionamentoModel> vagaEstacionamentoOptional = vagaEstacionamentoService.buscarPorId(id);

        if (vagaEstacionamentoOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Vaga de estacionamento inexistente.");
        }

        vagaEstacionamentoService.excluir(vagaEstacionamentoOptional.get());

        return ResponseEntity.status(HttpStatus.OK).body("Vaga de estacionamento excluída com sucesso.");
    }

}

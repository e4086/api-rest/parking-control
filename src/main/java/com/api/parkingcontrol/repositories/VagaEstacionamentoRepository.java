package com.api.parkingcontrol.repositories;

import com.api.parkingcontrol.models.VagaEstacionamentoModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;


public interface VagaEstacionamentoRepository extends JpaRepository<VagaEstacionamentoModel, UUID> {

    Boolean existsByPlacaVeiculo(String placaVeiculo);

    Boolean existsByNumero(String numero);

    Boolean existsByApartamentoAndBlocoApartamento(String apartamento, String blocoApartamento);
}

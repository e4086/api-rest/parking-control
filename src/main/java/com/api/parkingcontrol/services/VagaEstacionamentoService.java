package com.api.parkingcontrol.services;

import com.api.parkingcontrol.models.VagaEstacionamentoModel;
import com.api.parkingcontrol.repositories.VagaEstacionamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
public class VagaEstacionamentoService {

    @Autowired
    private VagaEstacionamentoRepository vagaEstacionamentoRepository;

    @Transactional
    public VagaEstacionamentoModel salvar(VagaEstacionamentoModel vagaEstacionamento) {
        return vagaEstacionamentoRepository.save(vagaEstacionamento);
    }

    public boolean jaExisteComEssaPlacaDeVeiculo(String placaVeiculo) {
        return vagaEstacionamentoRepository.existsByPlacaVeiculo(placaVeiculo);
    }

    public boolean jaExisteComEsseNumero(String numero) {
        return vagaEstacionamentoRepository.existsByNumero(numero);
    }

    public boolean jaExisteParaEsseBlocoNoApartamento(String apartamento, String blocoApartamento) {
        return vagaEstacionamentoRepository.existsByApartamentoAndBlocoApartamento(apartamento, blocoApartamento);
    }

    public Page<VagaEstacionamentoModel> buscarTodas(Pageable pageable) {
        return vagaEstacionamentoRepository.findAll(pageable);
    }

    public Optional<VagaEstacionamentoModel> buscarPorId(UUID id) {
        return vagaEstacionamentoRepository.findById(id);
    }

    @Transactional
    public void excluir(VagaEstacionamentoModel vagaEstacionamento) {
        vagaEstacionamentoRepository.delete(vagaEstacionamento);
    }
}

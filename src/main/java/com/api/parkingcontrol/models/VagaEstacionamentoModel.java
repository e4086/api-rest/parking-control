package com.api.parkingcontrol.models;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Table(name = "tb_vaga_estacionamento")
public class VagaEstacionamentoModel extends RepresentationModel<VagaEstacionamentoModel> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(name = "ve_id", nullable = false, unique = true, length = 10)
    private String numero;

    @Column(name = "ve_placa_veiculo", nullable = false, unique = true, length = 70)
    private String placaVeiculo;

    @Column(name = "ve_marca_veiculo", nullable = false, length = 70)
    private String marcaVeiculo;

    @Column(name = "ve_modelo_veiculo", nullable = false, length = 70)
    private String modeloVeiculo;

    @Column(name = "ve_cor_veiculo", nullable = false, length = 70)
    private String corVeiculo;

    @Column(name = "ve_data_registro", nullable = false)
    private LocalDateTime dataRegistro;

    @Column(name = "ve_responsavel", nullable = false, length = 130)
    private String nomeResponsavel;

    @Column(name = "ve_apartamento", nullable = false, length = 30)
    private String apartamento;

    @Column(name = "ve_bloco_apartamento", nullable = false, length = 30)
    private String blocoApartamento;

}

package com.api.parkingcontrol.dtos;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class VagaEstacionamentoDto {

    @NotBlank
    private String numero;

    @NotBlank
    @Size(max = 7)
    private String placaVeiculo;

    @NotBlank
    private String marcaVeiculo;

    @NotBlank
    private String modeloVeiculo;

    @NotBlank
    private String corVeiculo;

    @NotBlank
    private String nomeResponsavel;

    @NotBlank
    private String apartamento;

    @NotBlank
    private String blocoApartamento;
}
